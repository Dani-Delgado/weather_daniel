<?php
/**
 * Plugin Name: Wheather
 * Description: Daniel Delgado Wheather Plugin
 * Author:      Daniel Delgado
 * Version:     1.0
 */
if (defined('WP_CLI') && WP_CLI) {

function weather_command( $args , $city_args) {
    $city=$city_args['city'];
    

    $url='https://weatherapi-com.p.rapidapi.com/current.json?q='.$city;
    $arguments=array(
        'method'=>'GET',
        'headers' => array(
            'X-RapidAPI-Key' => 'ee61981762mshc359703d1550611p19cce4jsn52e1714df354',
            'X-RapidAPI-Host' => 'weatherapi-com.p.rapidapi.com',
          )
    );

    $response=wp_remote_get($url,$arguments);
    
    $content = json_decode(wp_remote_retrieve_body($response));

    if($content->current){
    $postContent = '
    <ul>
        <li><img src="' . $content->current->condition->icon . '" alt="'. $content->current->condition->text .'" /></li>
        <li>Humedad: ' . $content->current->humidity. '</li>
        <li>Temperatura:  ' . $content->current->feelslike_c. '</li>
    </ul>';

    wp_insert_post([
        'post_title' => 'Tiempo en ' . $city . ' ' . date("d/m/Y") ,
        'post_status' => 'publish',
        'post_content' =>  $postContent,
        ]);
        WP_CLI::success( 'Post creado con el clima de ' . $city );
    } else {
        WP_CLI::error( 'Ciudad no valida' );
    }
}
WP_CLI::add_command( 'weather forecast', 'weather_command' );
}